import { call, delay, fork, put, take } from 'redux-saga/effects';
import { getErr, getList, getTodo } from '../store/action/todoAction';
import { STATUS_CODE } from '../store/constant'
import * as type from './../store/types'

function* watchFetchListTaskAction() {
    // yield take(type.GET_TODO);
    console.log("getttt");
    const resp = yield call(getList);
    console.log("ress", resp);
    const { status, data } = resp;
    if (status === STATUS_CODE.SUCCESS) {
        console.log("chờ");
        yield delay(2000);
        yield put(getTodo(data));
        console.log("success");
    } else {
        yield put(getErr(data))
    }

    // console.log("Resp: " + resp);
}

function* rootSaga() {
    yield fork(watchFetchListTaskAction);
}

export default rootSaga;