import axios from "axios"
import { ADD_TODO, DELETE_TODO, GET_TODO, MARK_COMPLETE } from "../types"

export const markComplete = id => dispatch => {
    dispatch({
        type: MARK_COMPLETE,
        payload: id
    })
}

export const addTodo = newTodo => async dispatch => {
    try {
        await axios.post('https://jsonplaceholder.typicode.com/todos', newTodo)
        dispatch({
            type: ADD_TODO,
            payload: newTodo
        })
    } catch (error) {

    }
}

export const deleteTodo = id => async dispatch => {
    try {
        await axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
        dispatch({
            type: DELETE_TODO,
            payload: id
        })
    } catch {

    }
}
// export const fectchTodo = () => async dispatch => {
//     try {
//         const respon = await axios.get('https://jsonplaceholder.typicode.com/todos?_limit=3')

//     } catch (error) {
//         console.log(error);
//     }
// }

// export const getTodo = () => async dispatch => {
//     try {
//         const respon = await axios.get('https://jsonplaceholder.typicode.com/todos?_limit=3')
//         dispatch({
//             type: GET_TODO,
//             payload: respon.data
//         })
//     } catch (error) {
//         console.log(error);
//     }
// }

export const getList = () => {
    // try {
    //     const respon = await axios.get('https://jsonplaceholder.typicode.com/todos?_limit=3')
    //     dispatch({
    //         type: GET_TODO,
    //         payload: respon.data
    //     })
    // } catch (error) {
    //     console.log(error);
    // }
    return axios.get('https://jsonplaceholder.typicode.com/todos?_limit=3');
}

export const getTodo = data => {
    return {
        type: GET_TODO,
        payload: data
    }
}

export const getErr = error => {
    return {
        type: GET_TODO,
        payload: error
    }
}

export const filterTaskSuccess = data => ({
    
});