import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'; //sử dụng để gọi các hàm bất đồng bộ
import { composeWithDevTools } from 'redux-devtools-extension'
import rootReducer from './reducers';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../sagas';

const initialState = {}

const sagaMiddleware = createSagaMiddleware();

const configureStore = () => {
    const middleware = [thunk, sagaMiddleware];
    const enhancer = [applyMiddleware(...middleware)];
    const store = createStore(rootReducer, initialState, composeWithDevTools(...enhancer))
    sagaMiddleware.run(rootSaga);
    return store;
}

export default configureStore;
