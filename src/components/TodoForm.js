import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { addTodo } from '../store/action/todoAction';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
const TodoForm = ({ addTodo }) => {

    const [title, setTitle] = useState('')

    const onTitleChange = event => {
        setTitle(event.target.value)
    }

    const onFormSubmit = event => {
        event.preventDefault();

        if (title !== '') {
            const newTodo = {
                id: uuidv4(),
                title,
                completed: false
            }
            console.log(newTodo);
            addTodo(newTodo);
            setTitle('')
        }
    }

    return (
        <div>
            <form onSubmit={onFormSubmit}>
                <input type="text" value={title} name='title' onChange={onTitleChange} />
                <input type="submit" value="Add" />
            </form>
        </div>
    );
};

TodoForm.propTypes = {
    addTodo: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({})

export default connect(mapStateToProps, { addTodo })(TodoForm);
