import React, { useEffect } from 'react';
import TodoForm from './TodoForm';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { deleteTodo, getTodo, markComplete } from '../store/action/todoAction';


const Todos = ({ todos, markComplete, deleteTodo, getTodo }) => {

    // useEffect(() => {
    //     getTodo();
    // }, [])

    return (
        <div className="todo-list">
            <TodoForm />
            <ul>
                {todos.map(todo => (
                    <li key={todo.id} className={todo.completed ? 'completed' : ''}>
                        {todo.title}
                        <input type='checkbox' onChange={markComplete.bind(this, todo.id)} />
                        <button onClick={deleteTodo.bind(this, todo.id)}>Delete</button>
                    </li>))}
            </ul>
        </div>
    );
};

Todos.propTypes = {
    todos: PropTypes.array.isRequired,
    markComplete: PropTypes.func.isRequired,
    deleteTodo: PropTypes.func.isRequired,
    getTodo: PropTypes.func.isRequired
}

const mapStateToProps = (state) => {
    return {
        todos: state.myTodos.todos
    }
}

export default connect(mapStateToProps, { markComplete, deleteTodo, getTodo })(Todos);
